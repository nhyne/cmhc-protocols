#!/bin/sh

# generate jks file and put it where it needs to go
sleep 10

# openssl pkcs12 -export -in /ssl/server.crt -inkey /ssl/server.key \
#                -out server.p12 -name [localhost] \
#                -CAfile ca.crt -caname root


# keytool -importkeystore \
#         -deststorepass password -destkeypass password -destkeystore server.keystore \
#         -srckeystore server.p12 -srcstoretype PKCS12 -srcstorepass password \
#         -alias [localhost]

# actual entrypoint

/opt/jboss/keycloak/bin/add-user-keycloak.sh -u $ADMIN_USER -p $ADMIN_PASSWORD

/opt/jboss/keycloak/bin/standalone.sh -b 0.0.0.0 -Dkeycloak.migration.action=import -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.file=/opt/jboss/keycloak/import-data/realm-export.json -Dkeycloak.migration.strategy=IGNORE_EXISTING
