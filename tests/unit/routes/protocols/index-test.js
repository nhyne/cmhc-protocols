import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | protocols/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:protocols/index');
    assert.ok(route);
  });
});
