# Building ember
FROM node:9.5.0-slim AS ember

RUN apt-get -y update

WORKDIR /srv/www

COPY package.json .
COPY yarn.lock .

RUN yarn install

COPY . /srv/www

RUN npm run build --environment $NODE_ENV

# Building webserver

FROM nginx:1.13.9-alpine AS nginx

WORKDIR /srv/www

RUN rm /etc/nginx/conf.d/default.conf
COPY config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./docker-entrypoint.sh /srv/www/docker-entrypoint.sh

COPY config/nginx/http_server.conf.template /srv/www/config/nginx/http_server.conf.template
COPY config/nginx/https_server.conf.template /srv/www/config/nginx/https_server.conf.template

COPY --from=ember /srv/www/dist ./dist

ENTRYPOINT ["/bin/sh", "/srv/www/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
