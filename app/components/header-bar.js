import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  session: service('session'),
  signedIn: computed.reads('session.isAuthenticated'),

  actions: {
    logout() {
      this.get('session').invalidate('authenticator:oauth2');
    }
  }
});
