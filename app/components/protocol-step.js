import Component from '@ember/component';

export default Component.extend({
  actions: {
    removeStep(step) {
      this.send('removeStep', step);
    }
  }
});
