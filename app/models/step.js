import DS from 'ember-data';

export default DS.Model.extend({

  information: DS.attr('string'),
  imageLink: DS.attr('string'),
  stepNumber: DS.attr('number'),

  protocol: DS.belongsTo('protocol')
});
