import DS from 'ember-data';

export default DS.Model.extend({

  title: DS.attr('string'),
  description: DS.attr('string'),
  steps: DS.hasMany('step', {async: true}),

  pinRelationships() {
    let pinnedRelationships = {
      steps: this.get('steps').toArray()
    };
    this.set('_pinnedRelationships', pinnedRelationships);
  },

  rollbackRelationships() {
    let steps = this.get('_pinnedRelationships.steps');
    this.set('steps', steps);
  }
});
