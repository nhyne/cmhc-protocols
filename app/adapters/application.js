import JSONAPIAdapter from 'ember-data/adapters/json-api';
import { inject as service } from '@ember/service'
import { computed } from '@ember/object';

export default JSONAPIAdapter.extend({
  session: service('session'),
  keycloakAuthn: service('keycloak-authn'),

  namespace: 'api',

  headers: computed(function headers() {
    if (this.get('session.isAuthenticated')) {
      return {
        'Authorization': `Bearer ${this.get('keycloakAuthn.token')}`
      }
    }
  })
});
