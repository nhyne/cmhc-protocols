import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({

  stepCount: 1,

  protocol: computed.alias('model'),
  steps: computed(function steps() {
    return [];
  }),

  actions: {
    createProtocol() {
      this.get('protocol').save().then((savedParent) => {
        savedParent.get('steps').invoke('save')
      })
      .then(() => {
        this.transitionToRoute('protocols.show', this.get('protocol.id'));
        this.set('steps', []);
        this.set('stepCount', 1);
      });
    },

    createStep() {
      let step = this.store.createRecord('step', {
        information: '',
        protocol: this.get('protocol'),
        stepNumber: this.get('stepCount')
      });
      let currentSteps = this.get('steps');
      currentSteps.pushObject(step);
      let currentCount = this.get('stepCount');
      this.set('stepCount', currentCount + 1);
    },

    destroyStep(step) {
      let currentSteps = this.get('steps');
      currentSteps.removeObject(step);
    }
  }

});
