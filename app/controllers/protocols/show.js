import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import BufferedProxy from 'ember-buffered-proxy/proxy';

export default Controller.extend({
  session: service('session'),

  editing: false,

  protocol: computed.alias('model'),
  steps: computed.alias('model.steps'),
  signedIn: computed.reads('session.isAuthenticated'),

  bufferedProtocol: computed('protocol', function bufferedProtocol() {
    return BufferedProxy.create({
      content: this.get('protocol')
    });
  }),

  actions: {
    toggleEditing() {
      if (!this.get('editing')) {
        this.get('protocol').pinRelationships();
      }
      this.toggleProperty('editing');
    },

    saveProtocol() {
      this.get('bufferedProtocol').applyBufferedChanges();
      this.get('protocol').save().then((savedParent) => {
        savedParent.get('steps').invoke('save');
      });
      this.send('toggleEditing');
    },

    cancelEditing() {
      this.get('bufferedProtocol').discardBufferedChanges();
      this.get('protocol').rollbackRelationships();
      this.send('toggleEditing');
    },

    createStep() {
      let step = this.store.createRecord('step', {
        information: '',
        protocol: this.get('protocol')
      });
      this.get('steps').pushObject(step);
    },

    destroyStep(step) {
      this.get('steps').removeObject(step);
    }
  }
});
