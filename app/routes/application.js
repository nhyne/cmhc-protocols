import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  keycloakAuthn: service('keycloak-authn'),
  session: service('session'),

  async beforeModel() {
    let keycloakReady = this.get('keycloakAuthn').ready;
    if (!keycloakReady) {
      await this.get('keycloakAuthn').installKeycloak('/keycloak.json');
    }
    await this.get('keycloakAuthn').initKeycloak();
    if (!this.get('session.isAuthenticated')) {
      this.get('session').authenticate('authenticator:oauth2');
    }
  }
});
