import Base from 'ember-simple-auth/authenticators/base';
import { inject as service } from '@ember/service';
import RSVP from 'rsvp';

const { Promise } = RSVP;

export default Base.extend({
  keycloakAuthn: service('keycloak-authn'),

  authenticate() {
    return new Promise(async (resolve) => {
      if (!this.get('keycloakAuthn').authenticated) {
        await this.get('keycloakAuthn').login('/protocols');
      }
      resolve();
    });
  },

  invalidate() {
    return new Promise (async (resolve) => {
      await this.get('keycloakAuthn').logout();
      resolve();
    });
  }
});
