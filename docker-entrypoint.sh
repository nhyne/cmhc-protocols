if [ "$1" = 'nginx' ]; then
  if [ "$SSL_ENABLED" = 'true' ]; then
    cp /srv/www/config/nginx/https_server.conf.template /etc/nginx/conf.d/server.conf.template
  else
    cp /srv/www/config/nginx/http_server.conf.template /etc/nginx/conf.d/server.conf.template
  fi

  envsubst '$EMBER_HTTP_PORT,$EMBER_HTTPS_PORT' < /etc/nginx/conf.d/server.conf.template >| /etc/nginx/conf.d/server.conf
  envsubst '$KEYCLOAK_AUTH_URL' < /srv/www/dist/keycloak.json.template >| /srv/www/dist/keycloak.json
  nginx -t
fi

exec "$@"
